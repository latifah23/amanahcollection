<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat extends CI_Controller {

  public function index()
  {
  	$this->load->view('layouts/header');
    $this->load->view('lihat');
    $this->load->view('layouts/footer');
  }

}
