<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_pegawai extends CI_Controller {

  public function index()
  {
  	$this->load->view('layouts/header');
    $this->load->view('management_pegawai');
    $this->load->view('layouts/footer');
  }

}
