<!-- /.navbar -->
<div class="content">
	<div class="container">
		<section class="content-header">
		<div class="container-fluid">
		<div class="row mb-2">
                    <div class="col-sm-6">
                         <h1>Data Pesanan Masuk</h1>
                    </div>
                    <div class="col-sm-6 text-right">
                         <a href="" class="btn btn-primary btn-md">Tambah Costumer</a>
                    </div>
               </div>
		</div>
		</section>
		<div class="container-fluid">
			<div class="card card-primary">
				<div class="row">
					<!-- kolom kiri -->
					<div class="col-md-4">
						<!-- form start -->
						<form role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="nama">Nama Costumer</label>
									<input type="nama" class="form-control" id="nama" placeholder="Nama"></div>
								<!-- </div> -->
								<div class="form-group">
									<!-- Date -->
                                             <label for="nama">Tanggal Pemesanan</label>
									<div class="input-group input-daterange">
                                                  <input type="text" class="form-control" value="2012-04-05">
                                                  <div class="input-group-prepend">
                                                       <div class="input-group-text">To</div>
                                                  </div>
                                                  <input type="text" class="form-control" value="2012-04-19">
                                             </div>    
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-3">
						<!-- form start -->
						<form role="form">
							<div class="card-body">
								<div class="form-group">
									<label>Nama Pegawai</label>
									<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
										<option selected="selected" data-select2-id="3">Admin</option>
										<option data-select2-id="14">Sri</option>
										<option data-select2-id="15">Nur</option>
										<option data-select2-id="16">Heri</option>
										<option data-select2-id="17">Slamet</option>
									</select>
								</div>
								<div class="form-group">
									<label for="nomor">Kode Order</label>
									<input type="nomor" class="form-control" id="nomor" placeholder="Kode Order"></div>
							</div>
						</form>
					</div>
					<div class="col-md-4">
						<!-- kolom kanan -->
						<form role="form">
							<div class="card-body">
								<div class="form-group">
									<label>Kategori Produk</label>
									<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
										<option selected="selected" data-select2-id="3">PDL</option>
										<option data-select2-id="14">Polo Shirt</option>
										<option data-select2-id="15">T-Shirt</option>
										<option data-select2-id="16">Seragam olahraga</option>
										<option data-select2-id="17">Jaket</option>
										<option data-select2-id="18">Kemeja</option>
										<option data-select2-id="19">Topi</option>
										<option data-select2-id="20">Sabuk</option>
									</select>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>Jumlah design</label>
											<div class="input-group">
												<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
													<option selected="selected" data-select2-id="3">1</option>
													<option data-select2-id="14">2</option>
													<option data-select2-id="15">3</option>
													<option data-select2-id="16">4</option>
													<option data-select2-id="17">5</option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<!-- <label style="color:white">Jumlah design</label> -->
											<button style="margin-top:30px" type="submit" class="btn btn-primary">GO</button>
                                                       <a style="margin-top:30px" href="lihat" type="submit" class="btn btn-danger">Back</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
</div>