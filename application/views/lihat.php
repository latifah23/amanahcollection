<div class="container">
     <section class="content-header">
          <div class="container-fluid">
               <div class="row mb-2">
                    <div class="col-sm-6">
                         <h1>Data Pesanan Masuk</h1>
                    </div>
                    <div class="col-sm-6 text-right">
                         <a href="pesanan" class="btn btn-primary btn-md">Tambah Pesanan</a>
                    </div>
               </div>
          </div>
     </section>

     <!-- Main content -->
     <section class="content">
          <div class="card">
               <!-- /.card-header -->
               <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                         <thead>
                              <tr>
                              <th>Costumer</th>
                              <th>Produk</th>
                              <th>Kode Order</th>
                              <th>Pegawai</th>
                              <th>Tanggal Masuk</th>
                              <th>Tanggal Selesai</th>
                              <th>Status</th>
                              <th>Tindakan</th>
                              </tr>
                         </thead>
                         <tbody>
                         </tbody>
                    </table>
               </div>
          </div>
     </section>
</div>
